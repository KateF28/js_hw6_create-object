// ## Задание
// Реализовать функцию-конструктор для создания объекта "пользователь".

// #### Технические требования:
// - Написать функцию `createNewUser()`, которая будет создавать и возвращать объект "пользователь".
// - При вызове функция должна спросить у вызывающего имя и фамилию.
// - Используя данные, введенные пользователем, создать объект со свойствами `firstName` и `lastName`.
// - Добавить в объект метод `getLogin()`, который будет возвращать первую букву имени пользователя, 
// соединенную с фамилией пользователя, все в нижнем регистре.

let createNewUser = (firstNameProp, lastNameProp) => {
let newUser = new Object;
newUser.firstName = firstNameProp;
newUser.lastName = lastNameProp;
newUser.getLogin = () => {
	let combineStr = (newUser.firstName.substr(0, 1) + newUser.lastName).toLowerCase();
	return combineStr;
};
console.log(newUser.getLogin());
console.log(newUser);
};

let enteredName = String(prompt("Enter your name", "Name"));
let enteredLastName = String(prompt("Enter your surname", "Surname"));

createNewUser (enteredName, enteredLastName);


// Доп задание:
// Object.defineProperty(obj, 'name', {
// 	set: (val)=> {
// 		this.name = val;
// 	},
// 	get: () =>{return this.name},
// });
// obj.name= "ghgh"
// obj вызываем